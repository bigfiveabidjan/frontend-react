import { gql } from "@apollo/client";

/**
 * GraphQL categories and products query.
 */
const MENU_QUERY = gql`query {
  banniereTop: bannieresPub(where: {categoryId: 306}) {
    nodes {
      id
      title
      featuredImage {
        node {
          sourceUrl
          srcSet
        }
      }
    }
  }
  header: getHeader {
    favicon
    siteLogoUrl
    siteTagLine
    siteTitle
  }
  headerMenus: menuItems(where: {location: HCMS_MENU_HEADER, parentId: "0"}) {
    edges {
      node {
        id
        label
        url
        path
        childItems {
          edges {
            node {
              id
              label
              url
              path
              childItems {
                edges {
                  node {
                    id
                    label
                    url
                    path
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  footerMenus: menuItems(where: {location: HCMS_MENU_FOOTER, parentId: "0"}) {
    edges {
      node {
        id
        label
        url
        path
      }
    }
  }
  footer: getFooter {
    copyrightText
    sidebarOne
    sidebarTwo
    socialLinks {
      iconName
      iconUrl
    }
  }
}
`;

export default MENU_QUERY;
