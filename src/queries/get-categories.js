import { gql } from "@apollo/client";

/**
 * GraphQL categories query.
 */
const GET_CATEGORIES_QUERY = gql`query {

	productCategories {
		nodes {
			id
			name
			slug
			image {
				sourceUrl
				altText
			}
			parentDatabaseId
			products {
				nodes {
				  name
				}
			}
			  
		}
	}
	
}`;

export default GET_CATEGORIES_QUERY;
