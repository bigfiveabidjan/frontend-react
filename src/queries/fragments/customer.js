const CustomerFragment = `
    fragment CustomerFragment on Customer {
        id
        email
        firstName
        username
        lastName
    }
`;
export default CustomerFragment;