import { gql } from "@apollo/client";

export const PRODUCT_BY_SLUG_QUERY = gql` query Product($slug: ID!) {
	product(id: $slug, idType: SLUG) {
	  id
	  productId: databaseId
	  averageRating
      reviewCount
      reviewsAllowed
      reviews(first: 3) {
		edges {
			rating
			node {
			content
			date
			author {
				node {
				name
				... on User {
					id
					avatar {
					url
					}
					username
					registeredDate
				}
				}
			}
			}
		}
	  }
	  slug
	  description
	  related {
		nodes {
			id
			productId: databaseId
			averageRating
			slug
			description
			image {
				id
				altText
				sourceUrl
			}
			name
			... on SimpleProduct {
				price
				regularPrice
				id
			}
			... on VariableProduct {
				price
				id
				regularPrice
			}
			... on ExternalProduct {
				price
				id
				externalUrl
				regularPrice
			}
			... on GroupProduct {
				id
				products {
				nodes {
					... on SimpleProduct {
					id
					price
					regularPrice
					}
				}
				}
			}
		}
	  }
	  galleryImages {
		nodes {
		  id
		  title
		  altText
		  mediaItemUrl
		  authorId
		}
	  }
	  image {
		id
		uri
		title
		srcSet
		sourceUrl
		author {
		  node {
			avatar {
				url
			}
			firstName
			lastName
			username
			userId
			uri
			name
			databaseId
			email
			userId
		  }
		}
		authorDatabaseId
	  }
	  name
	  ... on SimpleProduct {
		price
		id
		regularPrice
		stockQuantity
	  }
	  ... on VariableProduct {
		price
		id
		regularPrice
		stockQuantity
	  }
	}
  }
  
`;

export const PRODUCT_SLUGS = gql` query Products {
  products(first: 5000) {
    nodes {
      id
      slug
    }
  }
}
`;
