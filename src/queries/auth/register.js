import { gql } from '@apollo/client';
import CustomerFragment from '../fragments/customer';

/**
 * Register user mutation query.
 */
export default gql`
    mutation RegisterMyUser($username: String!, $email: String!, $password: String!, $firstName: String!, $lastName: String!, $phone: String!, $city: String!) {
         registerCustomer(
            input: {
                clientMutationId: "CreateUser"
                email: $email
                firstName: $firstName
                lastName: $lastName
                password: $password
                username: $username
                billing: {phone: $phone, city: $city}
            }
        )   {
                customer {
                    ...CustomerFragment
                        billing {
                            phone
                            city
                        }
                }
           }
    }
    ${CustomerFragment}
`;