import { gql } from "@apollo/client";

/**
 * GraphQL categories and products query.
 */
const PRODUCTS_AND_CATEGORIES_QUERY = gql`query {
  sliders {
    nodes {
      slug
      uri
      featuredImage {
        node {
          sourceUrl
          srcSet
        }
      }
    }
  }
	productCategories {
		nodes {
			id
			name
			slug
			image {
				sourceUrl
				altText
			}
			parentDatabaseId
			products {
				nodes {
				  name
				}
			}
			  
		}
	}
  miseEnAvant: products(first: 10, where: {featured: true}) {
    nodes {
      id
      productId: databaseId
      averageRating
      slug
      description
      image {
        id
        altText
        sourceUrl
      }
      name
      ... on SimpleProduct {
        price
        regularPrice
        id
      }
      ... on VariableProduct {
        price
        id
        regularPrice
      }
      ... on ExternalProduct {
        price
        id
        externalUrl
        regularPrice
      }
      ... on GroupProduct {
        id
        products {
          nodes {
            ... on SimpleProduct {
              id
              price
              regularPrice
            }
          }
        }
      }
    }
  }
  products(first: 50) {
    nodes {
      id
      productId: databaseId
      averageRating
      slug
      description
      image {
        id
        altText
        sourceUrl
      }
      name
      ... on SimpleProduct {
        price
        regularPrice
        id
      }
      ... on VariableProduct {
        price
        id
        regularPrice
      }
      ... on ExternalProduct {
        price
        id
        externalUrl
        regularPrice
      }
      ... on GroupProduct {
        id
        products {
          nodes {
            ... on SimpleProduct {
              id
              price
              regularPrice
            }
          }
        }
      }
    }
  }
  banniereTop: bannieresPub(where: {categoryId: 306}) {
    nodes {
      id
      title
      featuredImage {
        node {
          sourceUrl
          srcSet
        }
      }
    }
  }
  banniereAfterSlider: bannieresPub(where: {categoryId: 305}) {
    nodes {
      id
      title
      featuredImage {
        node {
          sourceUrl
          srcSet
        }
      }
    }
  }
  header: getHeader {
    favicon
    siteLogoUrl
    siteTagLine
    siteTitle
  }
  headerMenus: menuItems(where: {location: HCMS_MENU_HEADER, parentId: "0"}) {
    edges {
      node {
        id
        label
        url
        path
        childItems {
          edges {
            node {
              id
              label
              url
              path
            }
          }
        }
      }
    }
  }
  footerMenus: menuItems(where: {location: HCMS_MENU_FOOTER, parentId: "0"}) {
    edges {
      node {
        id
        label
        url
        path
      }
    }
  }
  footer: getFooter {
    copyrightText
    sidebarOne
    sidebarTwo
    socialLinks {
      iconName
      iconUrl
    }
  }
}
`;

export default PRODUCTS_AND_CATEGORIES_QUERY;
