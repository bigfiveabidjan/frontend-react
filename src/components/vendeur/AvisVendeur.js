import React from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import Review from './Review';
import Commente from './Commente';

const AvisVendeur = (props) => {

    const averageRating = props.averageRating;
    const reviewCount = props.reviewCount;
    const produitReview = props.produit.reviews.edges;
    const vendeur = props.vendeur.node;
    return (
            <>
                <Col md="4">
                    <Row>
                        <Col md="5">
                            <img src={vendeur?.avatar.url} />
                        </Col>
                        <Col md="7">
                            <a><strong>{vendeur?.firstName ? vendeur?.firstName : '' + ' '+vendeur?.lastName ? vendeur?.lastName : ''}</strong>{'@'+vendeur?.name}</a>
                        </Col>
                    </Row>
                </Col>
                <Col md="8">
                    <h4>Avis pour</h4>
                    <Review vendeur={vendeur} averageRating={averageRating} reviewCount={reviewCount}/>
                    <Commente produitReview={produitReview}/>
                </Col>
            </>
        );
}

export default AvisVendeur;