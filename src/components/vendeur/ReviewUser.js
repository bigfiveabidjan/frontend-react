import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import moment from "moment";

moment.updateLocale('en', {
    relativeTime : {
        future: "%s",
        past:   "%s",
        s  : 'il y a quelques secondes',
        ss : 'il y a %d secondes',
        m:  "il y a une minute",
        mm: "il y a %d minutes",
        h:  "il y a une heure",
        hh: "il y a %d heures",
        d:  "il y a un jour",
        dd: "il y a %d jours",
        w:  "il y a une semaine",
        ww: "il y a %d semaines",
        M:  "il y a un mois",
        MM: "il y a %d mois",
        y:  "il y a un an",
        yy: "il y a %d ans"
    }
});

const ReviewUser = (props) => {

    const vendeur = props.vendeur;
    const averageRating = props.averageRating;
    const reviewCount = props.reviewCount;
    let date = new Date(props.commenteDate);
    const note = Math.round(averageRating);

    return (
            <>
                <a><strong>{vendeur?.firstName ? vendeur?.firstName : '' + ' '+vendeur?.lastName ? vendeur?.lastName : ''}</strong>{'@'+vendeur?.name}</a>
                <span className="date-commente"> " {moment(date, "YYYY-MM-DDTHH:mm:ss.sssZ").fromNow()}</span>
                <div className="vendor-review commente">
                    { 
                        note == 1 ? 
                        ( 
                            oneStar()
                        ) : ''
                    }
                    { 
                        note == 2 ? 
                        ( 
                            twoStar()
                        ) : ''
                    }
                    { 
                        note == 3 ? 
                        ( 
                            threeStar()
                        ) : ''
                    }
                    { 
                        note == 4 ? 
                        ( 
                            fourStar()
                        ) : ''
                    }
                    { 
                        note == 5 ? 
                        ( 
                            fiveStar()
                        ) : ''
                    }
                    
                </div>
            </>
        );
}

export default ReviewUser;

export const oneStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const twoStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const threeStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const fourStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const fiveStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
            </div>
        </div>
        )
    }