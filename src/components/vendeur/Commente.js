import React from 'react';
import { Col, Row } from 'react-bootstrap';
import ReviewUser from './ReviewUser';

const Commente = (props) => {
    const produitReview = props.produitReview;
    const reviewCount = null;
    return (
        <>
            {produitReview
                ? produitReview.map((avis, index) => (
                      <Row key={index}>
                          <Col md="2">
                              <img src={avis.node.author?.node.avatar.url} />
                          </Col>
                          <Col md="10">
                              <ReviewUser
                                  vendeur={avis.node.author?.node}
                                  averageRating={avis.rating}
                                  reviewCount={reviewCount}
                                  commenteDate={avis.node.date}
                              />

                              <div
                                  dangerouslySetInnerHTML={{
                                      __html: avis.node.content
                                  }}
                              />
                          </Col>
                      </Row>
                  ))
                : ''}
        </>
    );
};

export default Commente;
