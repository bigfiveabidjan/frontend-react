import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';

const Review = (props) => {

    const store_rating = props.store_rating;
    const note = Math.round(store_rating);

    return (
            <>
                <div className="vendor-review">
                    {store_rating == 0 ? "Il n’y a pas encore d’avis." : ""} 
                    { 
                        note == 1 ? 
                        ( 
                            oneStar()
                        ) : ''
                    }
                    { 
                        note == 2 ? 
                        ( 
                            twoStar()
                        ) : ''
                    }
                    { 
                        note == 3 ? 
                        ( 
                            threeStar()
                        ) : ''
                    }
                    { 
                        note == 4 ? 
                        ( 
                            fourStar()
                        ) : ''
                    }
                    { 
                        note == 5 ? 
                        ( 
                            fiveStar()
                        ) : ''
                    }                    
                </div>
            </>
        );
}

export default Review;

export const oneStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const twoStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const threeStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const fourStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
                <FontAwesomeIcon icon={faStar} />
            </div>
        </div>
        )
    }
export const fiveStar = () => {
    return (
        <div className="inline-block ">
            <div className="optenu inline-block ">
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
                <FontAwesomeIcon icon={faStar} />
            </div>
            <div className="reste inline-block ">
            </div>
        </div>
        )
    }