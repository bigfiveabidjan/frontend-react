import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Button, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComment } from '@fortawesome/free-solid-svg-icons';
import Review from './Review';
import axios from 'axios';

const token = 'Basic YWRtaW46YWRpbmtyYTIyNQ==';

const ContactVendeur = (props) => {
    let vendeur = props.vendeur;
    const averageRating = props.averageRating;
    const reviewCount = props.reviewCount;
    const productId = props.productId;  

  //console.warn(vendeur);
    return (
        <>
        <Container className="contactVendor">
          <Row>
              <Col md={12} className="vendor-info">
                  <Row className="justify-content-md-center">
                      <Col md={3} className="vendro-avatar">
                        <img src={vendor?.vendor_shop_logo} />
                      </Col>
                      <Col md={9} className="vendor-name">
                        <Review vendeur={vendeur} averageRating={averageRating} reviewCount={reviewCount}/>
                      </Col>
                      <Col md={12}>
                        <Button variant="danger"> <FontAwesomeIcon icon={faComment} /> Chat</Button>
                      </Col>
                  </Row>
              </Col>
              <Col md={12} className="contact-form-vendeur">
                  <Row className="justify-content-md-center">
                      <Col md={12} className="">
                        <Form>
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                <Form.Control placeholder="Ecrivez votre demande" as="textarea" rows={3} />
                            </Form.Group>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Control type="email" placeholder="Votre email" />
                            </Form.Group>
                            <Form.Group controlId="formBasicCheckbox">
                                <Form.Control type="text" placeholder="Votre nom" />
                            </Form.Group>
                            <Button variant="warning" type="submit">
                                Envoyez le message à {'@'+vendor?.name}
                            </Button>
                        </Form>
                      </Col>
                  </Row>
              </Col>
          </Row>
        </Container>
          
  
        </>
      );
}

export default ContactVendeur;
