import { isEmpty, isArray } from 'lodash';
import {useState, useRef} from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const GalleryCarousel = ({gallery, imageprod}) => {
    if ( isEmpty(gallery) || ! isArray( gallery ) ) {
        return null;
    }

    const activeIndexRef = useRef( { activeIndex: 0 } );
    const slideRef = useRef( 0 );
    const [ slide, setSlide ] = useState( 0 );
    const [ restartSlide, setRestartSlide ] = useState( 0 );
    const { activeIndex } = activeIndexRef.current;

    /**
     * Change to next slide.
     */
  /*  const nextSlide = () => {

        if ( 1 === gallery.length ) {
            return null;
        }

      
        if ( activeIndexRef.current.activeIndex === gallery.length - 1 ) {

            activeIndexRef.current.activeIndex = 0;
            setRestartSlide( restartSlide + 2 );

        } else {

            // If its not the last slide increment active index by one.
            activeIndexRef.current.activeIndex =
                activeIndexRef.current.activeIndex + 2;

        }

        slideRef.current = slideRef.current + 2;
        setSlide( slideRef.current );

    };*/
    var settings = {
        dots: true,
        infinite: true,
        autoplay:true,
        arrow:true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
      };
    return (
          <div className="gal">
            <Slider {...settings}>
               
                {
                   
                    gallery.map( ( item, index ) => {
                        const opacity = ( activeIndex === index || 1 === gallery.length ) ? 'opacity-100' : 'opacity-100';
                        return (
                            <div key={item?.id} className={`${opacity}`}>
                                <img
                                    src={item?.mediaItemUrl} loading="lazy" alt={ item?.altText ? item?.altText : item?.title }
                                />
                            </div>
                        )
                    })
                }
               
                </Slider>
          </div>
    )
}

export default GalleryCarousel
