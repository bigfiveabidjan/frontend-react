import Link from 'next/link';
import React , {Component,useEffect,useState} from 'react';
import client from '../components/ApolloClient';
import { gql } from "@apollo/client";
import  MENU_QUERY  from "../../src/queries/get-menu";
import CartIcon from "./cart/CartIcon";
import { Warning } from 'postcss';
import { isUserValidated, logoutUser } from '../utils/auth-functions';
import isEmpty from '../validator/isEmpty';
import {Container,  Row,  Col,  Navbar, Nav, NavDropdown} from 'react-bootstrap'
import Search from "../components/Search";
import Router from 'next/router';

class Menu extends Component {



	state = {
		loggedIn:false,
		showContent:false,
		userData:'',
		searchFilter:'',

	};
	handleLogout = () => {
        if (process.browser) {
            logoutUser('/login');
        }
    };


	setStateAsync(state) {
		return new Promise((resolve) => {
		  this.setState(state, resolve)
		  
		});
	  }
     componentDidMount() {
		

		if (process.browser) {
            const userValidated = isUserValidated();
           
            // If user is not validated, then logout button should be shown.
            if (!isEmpty(userValidated)) {
                this.setState({loggedIn: true});
				this.setState({showContent: true});
				this.setState({userData: userValidated});
            }
        }
     	this.renderPosts();
     }
  
	renderPosts = async() => {
			try {
				
			const res = await client.query( {
				query: MENU_QUERY,
			} );
			const posts = res.data;
			
			{
				posts.headerMenus.edges.map( ( item) => {				
				})
			}
			
			// this will re render the view with new data
		 this.setState({
				mydata: posts
			});	/* */
		
			} catch (err) {
			
			}
		}
  
		render() {
			

	return (

<> 

<Container>
		<Row>
			<Col xs={12} md={2}>
					<span className="font-semibold text-xl tracking-tight">
									<Link href="/">
									<a className="logo">
								
									<img  src={this.state.mydata?.header.siteLogoUrl} alt="Logo" className="mr-4"/>
									</a>
									</Link>
							
					</span>	
			</Col>
			<Col xs={12} md={7}>
						<div className="container pub-top">
								<div className="bannierePubHaut">
									<img  src={this.state.mydata?.banniereTop.nodes[0].featuredImage?.node.sourceUrl} alt="Baniere Pub Haut" />
								</div>
						</div>
			</Col>
			<Col xs={12} md={3}>
					<div className="s">	
					{this.state.loggedIn ? (
										<ul className="compt-m my-menu">
											<li>
												<Link href="/my-account">
													<a aria-haspopup="true"	className="mon-compte">
													<svg xmlns="http://www.w3.org/2000/svg" className="hidden lg:block m-auto" fill="none" viewBox="0 0 24 24" width="18" height="18" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" /></svg>
											
														Bonjour, {this.state.userData.user?.name}
													</a>	
																		
												</Link> 	
												<ul className="mega-compte">
														<li>
															<Link href="/my-account">
																<a aria-haspopup="true">
																	Mon compte
																</a>	
																					
															</Link> 	
														</li>
														<li>
															<a className="deconn" onClick={this.handleLogout}>	Se déconnecter	</a>
														</li>
														
													</ul>
											</li>
									   	
											</ul>	
								  
									
								) : (
								<ul className="compt-m my-menu">
								    <li>
										<span>
										<Link href="/login">
											<a>
												<svg xmlns="http://www.w3.org/2000/svg" className="hidden lg:block m-auto" fill="none" viewBox="0 0 24 24" width="18" height="18" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" /></svg>
											Se connecter</a>
										</Link>
										</span>
											{/* <span className="wpd-main-nav__list">
											<Link href="/register">
												<a className="wpd-main-nav__link">S'enregistrer</a>
											</Link>
								     	</span> */}	
								    </li> 
						         </ul>			  
						)
						} 
					
						<CartIcon/>
					</div>
					
			</Col>
		</Row>
		
</Container>

<Navbar >
<Container>
	
	<Navbar.Toggle aria-controls='responsive-navbar-nav'></Navbar.Toggle>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
     
	<Navbar.Collapse id='responsive-navbar-nav'>
		<Nav >
			<Col md={12}>
			 <a href="javascript:void(0);" className="mobile-menu-trigger">
				 <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
						<title>ADINKRA</title>
						<path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
					</svg>
			</a>
			
					<ul className="menu menu-bar  ">
						

							<li className="mobile-menu-header">
								<a href="#" className="">
									<span>ADINKRA</span>
								</a>
							</li>
						
									{ this.state.mydata?.headerMenus.edges ? (
								
								this.state.mydata?.headerMenus.edges.map( post => 

							{	let verif = "true"; 
							    post.node.childItems.edges.length ? verif : verif="false";	
								
								return (
										<li key={post.node.id} href={post.node.path}>	
									
											<Link key={post.node.id} href={post.node.path} >
												<a  aria-haspopup={verif}
													className="menu-link menu-bar-link"
													data-cy="nav-item"
												>
													{post.node.label}
												</a>
											</Link>
											<ul className="mega-menu mega-menu--multiLevel">
												{ post.node.childItems.edges.length ? (
													<>
														{ post.node.childItems.edges.map( subMenu  => {
															const thePath = subMenu.node.path.split('/');
															const getLastItem = thePath[thePath.length - 2];

														return (
														<li key={subMenu.node.id} >	
																	<Link key={subMenu.node.id} 
																	      className="header-nav__submenu-link"
																			href={'/category/'+getLastItem} >

																		<a 	aria-haspopup={subMenu.node.childItems.edges.length ? "true" : verif="false"}
																				to={ `${ '/category/'+getLastItem }`}
																				className="menu-link mega-menu-link"> 
																		{ subMenu.node.label }
																			</a>
																	</Link>
																	<ul className="menu menu-list">
																		
																		{ subMenu.node.childItems.edges.length ? (
																			   <>
																				   { subMenu.node.childItems.edges.map( subSubMenu  => {
																						const thePath = subSubMenu.node.path.split('/');
																						const getLastItem = thePath[thePath.length - 2];

																				   return (
																				<li key={subSubMenu.node.id}>		   
																							   <Link key={subSubMenu.node.id} 
																							         href={'/category/'+getLastItem}
																									className="header-nav__submenu-link"
																									to={ `${ '/category/'+getLastItem }`}
																							   > 
																							   <a className="menu-link menu-list-link">{ subSubMenu.node.label }</a>
																								   
																							   </Link>
																							   
																					 </li>  )
																				    } ) }
																			   </>
																		   ) : "" }
																		  
																	   </ul>
																		
																	   
																	
														</li>	)
														} ) }
													</>
												) : null }
								        	</ul>


								           
									</li>
								
									)}
								
									)
							) : '' }
						<li className="devseller">
							<Link
								href="https://adinkra.bigfive.dev/dashboard"
								
							>Vendre sur ADINGRA
							</Link>
					   </li>
                    
				</ul>  		
			</Col>
		</Nav>
		</Navbar.Collapse>	
		<div className="Search">
			<div className="input-group">
					<button type="button" className="btn btn-primary"
						onClick={() => {
							Router.push('/search')
						}}
					>
						<i className="fas fa-search">Recherche</i>
					</button>
				</div>
			</div>
		</Container>
		</Navbar>

		</>
	);
		}
}
export default Menu;

const searchWord = (word) => {
	return productSearch(word);
}

const productSearch = (name) => {
	return name;
}