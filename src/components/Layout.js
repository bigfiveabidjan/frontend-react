import Head from "next/head";
import { AppProvider } from "./context/AppContext";
import { useState, useEffect } from 'react';
import Footer from "./Footer";
import client from "./ApolloClient";
import Router from "next/router";
import NProgress from "nprogress";
import { ApolloProvider } from "@apollo/client";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());
import Menu from "./Nav";



const Layout = (props) => {

  const {header, headerMenus, footerMenus, footer, titre} = props || {};
  useEffect(() => {
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', function () {
            navigator.serviceWorker
                .register('/service-worker.js', { scope: '/' })
                .then(function (registration) {
                    // SW registered
                })
                .catch(function (registrationError) {
                    // SW registration failed
                });
        });
    }
}, []);

  return (
    <AppProvider>
      <ApolloProvider client={client}>
        <div>
          <Head>
            <title>
              { titre ? titre : 'ADINKRA ' }
            </title>
          </Head>
          <div className="header">
            <Menu  />
          </div>
          {props.children}
          <Footer footer={footer} footerMenus={footerMenus?.edges}/>
        </div>
      </ApolloProvider>
    </AppProvider>
  );
}; 

export default Layout;
