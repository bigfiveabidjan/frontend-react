import Link from 'next/link';
import CartIcon from "../cart/CartIcon";
import {useState, useEffect, useRef} from 'react';
import { gql } from "@apollo/client";


const fetchURL = "http://adinkra.bigfive.dev/wp-json/custom-routes/v1/bannire-top";
const getItems = () => fetch(fetchURL).then(res => res.json());
function List({ items, fallback }) {
    if (!items || items.length === 0) {
      return fallback;
    } else {
      return items.map((item, index) => {
        return <img key={index} src={item.featured_image?.url}></img>;
      });
    }
  }
  
  function BannerPub() {
    const [items, setItems] = useState([]);
  
    useEffect(() => {
      getItems().then(data => setItems(data));
    }, []);
  
    return (
      <div>
        <List items={items} fallback={"Chargement..."} />
      </div>
    );
  }

const TopHeader = () => {
    return (
        <>
            <div className="flex items-center justify-between flex-wrap container mx-auto">
                <div className="flex items-center flex-shrink-0 text-black mr-20">
                <span className="font-semibold text-xl tracking-tight">
                    <Link href="/">
                    <a className="">Adinkra</a>
                    </Link>
                    </span>
                </div>

                 <div className="text-sm font-medium">
                        <div className="container pub-top">
                        <BannerPub />
                        </div>
                   
				</div>

        <div className="text-sm font-medium">
						<a href="#responsive-header" className="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-black mr-10">
						<svg xmlns="http://www.w3.org/2000/svg" className="hidden lg:block m-auto" fill="none" viewBox="0 0 24 24" width="18" height="18" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" /></svg>
							Profil
						</a>
						<a href="#responsive-header" className="block mt-4 lg:inline-block lg:mt-0 text-black hover:text-black mr-10">
						<svg xmlns="http://www.w3.org/2000/svg" className="hidden lg:block m-auto" fill="none" viewBox="0 0 24 24" width="18" height="18" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" /></svg>
							Souhait
						</a>
						<CartIcon/>
					</div>
        </div>
        </>
    )
}

export default TopHeader;