import Link from 'next/link';

const BanniereHome = ( props ) => {
	const { bannierePub } = props;

	return (
		    <div className="container mx-auto pub px-4 xl:px-0">
              {
                  bannierePub.map( ( item) => {
                    
                      return (
                          <div key={item.id} className="banner-pub grid grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-1 gap-1">
                              <img
                                  src={item?.featuredImage?.node.sourceUrl} srcSet={item?.image_banniere} loading="lazy"
                              />
                          </div>
                      )
                  })
              }
            </div>
	);
};

export default BanniereHome;