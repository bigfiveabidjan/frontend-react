import { isEmpty, isArray } from 'lodash';
import Link from "next/link";
import {useState, useEffect, useRef} from 'react';

const BannerPub = (bannierePub) => {

    if ( isEmpty(bannierePub) || ! isArray( bannierePub ) ) {
    	return null;
    }
    return (
  
            <div className="container mx-auto pub px-4 xl:px-0">
                {
                    bannierePub.map( ( item) => {
                      
                        return (
                            <div key={item.id} className="banner-pub grid grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-1 gap-1">
                                <img
                                    src={item?.featured_image?.url} srcSet={item?.featured_image?.url} loading="lazy"
                                />
                            </div>
                        )
                    })
                }
               
            </div>
       
    )
}

export default BannerPub;
