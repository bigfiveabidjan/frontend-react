import axios from "axios";
import React, { Component } from 'react';
import { isEmpty, isArray } from 'lodash';
import Link from "next/link";
import {useState, useEffect, useRef} from 'react';
import BannerPub from "./BannerPub";

class PubBanner extends Component {
    constructor(props){
      super(props);
      this.state = {
        posts:[]
      }
    }
  
    componentDidMount = async () => {
      await axios.get(
        'https://adinkra.bigfive.dev/wp-json/custom-routes/v1/bannire-pub'
        ).then(post =>{
          this.setState({posts : post.data});
        }).catch(error => {
          console.log(error);
        });
    }
  
    removeTags = (str) => {
        if ((str===null) || (str===''))
        return false;
        else
        str = str.toString();
        return str.replace( /(<([^>]+)>)/ig, '');
     }

    
    render(){
        return (
            <BannerPub data={this.state.posts} />

        );
    }
  }

  export default PubBanner;