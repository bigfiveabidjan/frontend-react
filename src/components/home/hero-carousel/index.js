import axios from "axios";
import React, { Component } from 'react';
import { isEmpty, isArray } from 'lodash';
import Link from "next/link";
import {useState, useEffect, useRef} from 'react';
import Carousel from "./Carousel";

const HeroCarousel = ( {sliders} ) => {

	return (
    <Carousel data={sliders} />
  );
        
}

export default HeroCarousel;