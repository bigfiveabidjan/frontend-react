import React, { useState } from 'react';
import { useLazyQuery } from '@apollo/client';
import gql from 'graphql-tag';
import { Warning } from 'postcss';
import Product from "./Product";
import { Col, Row, Container } from 'react-bootstrap';

const FEED_SEARCH_QUERY = gql`
  query FeedSearchQuery($filter: String!) {
    products(where: {search: $filter}) {
        nodes {
        id
        productId: databaseId
        averageRating
        slug
        description
        image {
            id
            altText
            sourceUrl
        }
        name
        ... on SimpleProduct {
            price
            regularPrice
            id
        }
        ... on VariableProduct {
            price
            id
            regularPrice
        }
        ... on ExternalProduct {
            price
            id
            externalUrl
            regularPrice
        }
        ... on GroupProduct {
            id
            products {
            nodes {
                ... on SimpleProduct {
                id
                price
                regularPrice
                }
            }
            }
        }
    }
    }
  }
`;

const Search = (props) => {
    const [searchFilter, setSearchFilter] = useState('');
    const [executeSearch, { data }] = useLazyQuery(
      FEED_SEARCH_QUERY
    );
    return (
      <>
      <div className="rec">
      <Container>
        <Row className="justify-content-md-center">
                    <Col md={12}>
                        <div className="input-group">
                            <div className="form-outline">
                                <input type="search" id="form1" className="form-control" 
                                    onChange={
                                        (e) => {
                                            setSearchFilter(e.target.value)
                                            executeSearch({variables: { filter: searchFilter }})
                                        }
                                    }
                                    placeholder="Que recherchez-vous ?"
                                />
                            </div>
                            <button type="button" className="btn btn-primary"
                                onClick={() =>
                                executeSearch({
                                    variables: { filter: searchFilter }
                                })
                                }
                            >
                                <i className="fas fa-search">Rechercher</i>
                            </button>
                        </div>
                    </Col>
                </Row>
        </Container>
    </div>
      <Container className="searchPage">
        <Row>
            <Col md={12}>
              
                <Row className="searchResult">
                            <Col md="12">
                                <p>Résultat(s) de recherche pour : {searchFilter}</p>
                            </Col>      
                    {data?.products.nodes.length > 0
                    ? data.products.nodes.map((product, index) => {
                        return (
                            <Col md={3} key={index}>                                { 
                                    <Product key={ product.id } product={ product }/> 
                                }
                            </Col>
                        )
                    })
                    : <p>Aucun résultat</p>}
                </Row>

                <div>

                </div>
            </Col>
        </Row>
      </Container>
        

      </>
    );
  };

export default Search;