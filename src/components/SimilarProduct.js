import React from 'react';
import Product from "./Product";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SimilarProduct = (props) => {
  const products = props.products; 

  var settings = {
    dots: true,
    infinite: true,
    autoplay:true,
    arrow:true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  };
  return (
        <div className="similar">
             <Slider {...settings}>
						{products.length ? (
							products.map( product => <Product key={ product.id } product={ product }/> )
						) : '' }
            </Slider>
				</div>
      );
}

export default SimilarProduct;