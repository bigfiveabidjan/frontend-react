import Link from 'next/link';
import Image from "../../../image";
import {DEFAULT_CATEGORY_IMG_URL} from "../../../constants/urls";
import { LazyLoadImage } from 'react-lazy-load-image-component';

const ParentCategoryBlock = ( props ) => {

	const { category } = props;

	return (
		<>
		{ (category?.parentDatabaseId == null)?
			 ('')
			:
		<div className="product mb-5">
			<Link href={`/category/${category?.slug}`}>
				<a className="prodhome">
				<LazyLoadImage
				className="object-cover h-40 md:h-64"
				layout="fill"
				containerclassnames="w-96 h-56"
				src={ category?.image?.sourceUrl ?? '' }
				placeholderSrc={DEFAULT_CATEGORY_IMG_URL}
				threshold='100'
				alttext={category?.image?.altText ?? category.slug}
    
       />
				
					<div className="product-title-container p-3">
						<h3 className="product-title text-lg font-medium">{category?.name}</h3>
						<span className="shop-now text-sm">+ Explore</span>
					</div>
				</a>
			</Link>
		</div>
		}
</>
	);
}

export default ParentCategoryBlock;
