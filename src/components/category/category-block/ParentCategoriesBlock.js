import ProductCategoryBlock from "./ParentCategoryBlock";

const ParentCategoriesBlock = ( props ) => {

	const { productCategories } = props || {};
	return (
		<div className="product-categories grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-4 xl:grid-cols-4 gap-4">
				{productCategories.map( 
					( productCategory, index ) => 
						<ProductCategoryBlock key={ productCategory?.id ?? index }  category={ productCategory }/>

						
				)}
		</div>
	)

};

export default ParentCategoriesBlock;
