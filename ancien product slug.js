import Layout from '../../src/components/Layout';
import { useRouter } from 'next/router';
import client from '../../src/components/ApolloClient';
import AddToCartButton from '../../src/components/cart/AddToCartButton';
import {PRODUCT_BY_SLUG_QUERY, PRODUCT_SLUGS} from '../../src/queries/product-by-slug';
import { isEmpty } from 'lodash';
import GalleryCarousel from "../../src/components/single-product/gallery-carousel";
import Price from "../../src/components/single-product/price";
import React, { Component } from 'react';
import axios from 'axios';
import Image from 'next/image';
import Link from 'next/link';

class Product extends Component {

    constructor (props) {
        super(props);
        this.state = {
            posts:[],
            authors:[]
        }
    }

    componentDidMount = async () => {
        await axios.get(
          'http://adinkra.bigfive.dev/wp-json/custom-routes/v1/products', { params: { id: this.props.product.productId } }
          ).then(post =>{
            this.setState({posts : post.data});
          }).catch(error => {
            console.log(error);
          });

          await axios.get(
            'https://adinkra.bigfive.dev/wp-json/wp/v2/users',{ params: { id: this.state.posts[0]?.author?.id} }
            ).then(post =>{
              this.setState({authors : post.data});
            }).catch(error => {
              console.log(error);
            });
      }
    
      removeTags = (str) => {
          if ((str===null) || (str===''))
          return false;
          else
          str = str.toString();
          return str.replace( /(<([^>]+)>)/ig, '');
       }

    render() {
        // Get it from props
        const { route } = this.props;
        const product = this.props.product;
        const vendeur = this.state.posts[0]?.author;
        const auteurs = this.state.authors;
        const products = this.props.product?.productCategories?.nodes;
        const data = this.props.data;
        console.warn(data);
        return (
            <Layout titre={product.name}>
			{ product ? (
				<div className="single-product container mx-auto my-32 px-4 xl:px-0">
					<div className="grid md:grid-cols-3 gap-4">
						<div className="product-infos col-span-2">
                            <div className="product-images">
                                { !isEmpty( product?.galleryImages?.nodes ) ? (
                                    <GalleryCarousel gallery={product?.galleryImages?.nodes}/>
                                ) : !isEmpty( product.image ) ? (
                                    <img
                                        src={ product?.image?.sourceUrl }
                                        alt={ product.name }
                                        width="100%"
                                        height="auto"
                                        srcSet={ product?.image?.srcSet }
                                    />
                                ) : null }
                            </div>

                            <div className="product-info">
                                <h4 className="products-main-title text-2xl uppercase">{ product.name }</h4>
                                
                                <Price salesPrice={product?.price} regularPrice={product?.regularPrice}/>  
                            
                                <div className="grid md:grid-cols-1 gap-1"

                                    dangerouslySetInnerHTML={ {
                                        __html: product.description, 
                                    } }
                                    className="product-description mb-5"
                                />
                            
                                {/*<AddToCartButton product={ product }/>*/}  
                            </div>
						</div> 
                        <div className="product-info author-info">
                            <h3>{product.stockQuantity ? product.stockQuantity+' en stock' : '' }   </h3>
                            <AddToCartButton product={ product }/>  
                            

                        <div className="">
                            { vendeur ? (
                                <>
                                    {/*Image du vendeur*/}
                                    { auteurs ? (
                                        
                                        auteurs.map(auteur => {
                                            if(auteur.id === vendeur.id)
                                                return  <a key={auteur.id}><img src='https://www.clipartkey.com/mpngs/m/29-297748_round-profile-image-placeholder.png' /></a>
                                        }) 
                                    ) : 'rien' }

                                    <h2>
                                    {vendeur?.display_name}
                                    
                                    </h2>
                                    <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                                        <a href={"mailto:"+vendeur?.email}>Envoyer un message</a>
                                    </button>
                                    <button className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow" disabled>
                                    <a href={'#'/*vendeur?.link*/} >Voir les autres produits de ce vendeur</a>
                                    </button>
                                    
                                </>
                            ) : ''
                            
                            }
                            
                        </div>

                        </div>
						
                        {/*Products similar*/ }
                        
					</div>
                    <div className="grid md:grid-cols-1 gap-4">
                    <div className="products container mx-auto my-32 px-4 xl:px-0">
                            <h2 className="products-main-title main-title mb-4 text-xl uppercase"><span className="main-title-inner">Produits similaires</span></h2>
                                {/*products[0]?.name products?.map( product => <Product key={ product.id } product={ product }/> ) */}
                                
                                {products? (
                                    products?.map( (item, index) => {
                                        
                                        return (
                                            <div key={index}>
                                                <div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 gap-4">
                                                {item.products.nodes?.length ? (
                                                    item.products.nodes?.map( (produit) => {
                                                        if (product.name !== produit.name) {  
                                                            return (
                                                                    <div key={produit.id} className="product mb-4">
                                                                        <Link href={ `/product/${ produit?.slug }`} >
                                                                            <a>
                                                                                <img
                                                                                    src={ produit?.image?.sourceUrl }
                                                                                    alt={ produit.name }
                                                                                    width="100%"
                                                                                    height="auto"
                                                                                    srcSet={ produit?.image?.srcSet }
                                                                                />
                                                                            </a>
                                                                        </Link>
                                                                        <div className="product-info">
                                                                            <h3 className="product-title mt-3 font-medium text-gray-800">
                                                                                { produit.name ? produit.name : '' }<br/>
                                                                                <span dangerouslySetInnerHTML={{ __html: (produit.price)}}/>
                                                                            </h3>
                                                                            {/*<div className="product-description text-sm text-gray-700" dangerouslySetInnerHTML={{ __html: (product?.description)}}/>*/}
                                                                            <AddToCartButton product={ produit }/>
                                                                        </div>

                                                                    </div>
                                                                )
                                                        }
                                                    })
                                                ) : ''}
                                                </div>
                                            </div>
                                            
                                            
                                        )} )
                                ) : <p>Aucun produit similaire</p> }
                            
                        </div>

					</div>

				</div>
			) : (
				''
			) }
		</Layout>
        )
    }
};

const product = (props) => {
    const { product, products } = props;
    const router = useRouter();

    if (router.isFallback) {
        return <div>Chargement...</div>
    }
  
    return <Product {...props} router={router} />;
};
export default  product;

export async function getStaticProps(context) {

    const {params: { slug }} = context

    const {data} = await client.query({
        query: PRODUCT_BY_SLUG_QUERY,
        variables: { slug }
    })

    return {
        props: {
            product: data?.product || {},
            products: data?.product?.productCategories?.nodes || {},
            data: data,
        },
        revalidate: 1
    };
}

export async function getStaticPaths () {
    const { data } = await client.query({
        query: PRODUCT_SLUGS
    })

    const pathsData = []

    data?.products?.nodes && data?.products?.nodes.map((product) => {
        if (!isEmpty(product?.slug)) {
            pathsData.push({ params: { slug: product?.slug } })
        }
    })

    return {
        paths: pathsData,
        fallback: true
    }
}