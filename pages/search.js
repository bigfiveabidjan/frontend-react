import Search from "../src/components/Search";

import Layout from "../src/components/Layout";
import Product from "../src/components/Product";
import client from '../src/components/ApolloClient';

export default function Recherche (props) {

	const {  products, productCategories, miseEnAvant, sliders, bannierePub, header, headerMenus, footerMenus, footer} = props || {};

	return (
		<div>
        <Layout>
            <Search />
        </Layout>
		</div>
	)
};

