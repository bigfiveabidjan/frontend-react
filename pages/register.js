import Layout from '../src/components/Layout';
import { useState } from 'react';
import client from '../src/apollo/ApolloClient';
import { useMutation } from '@apollo/client'
import MessageAlert from '../src/components/message-alert/MessageAlert';
import Loading from '../src/components/message-alert/Loading';
import Router from 'next/router';
import { isUserValidated } from '../src/utils/auth-functions';
import isEmpty from '../src/validator/isEmpty';
import Link from 'next/link';
import validateAndSanitizeRegisterForm from '../src/validator/register';
import { REGISTER_USER } from '../src/queries';
import Container from 'react-bootstrap/Container';

/**
 * Register Functional Component.
 *
 * @return {object} Register form.
 */
const Register = () => {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [phone, setPhone] = useState('');
    const [city, setCity] = useState('');

    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');
    const [showAlertBar, setShowAlertBar] = useState(true);

    // Check if the user is validated already.
    if (process.browser) {
        const userValidated = isUserValidated();

        // Redirect the user to My Account page if user is already validated.
        if (!isEmpty(userValidated)) {
            Router.push('/my-account');
        }
    }

    /**
     * Hide the Status bar on cross button click.
     */
    const onCloseButtonClick = () => {
        setErrorMessage('');
        setShowAlertBar(false);
    };

    /**
     * Sets client side error.
     *
     * Sets error data to result of our client side validation,
     * and statusbars to true so that its visible.
     *
     * @param {Object} validationResult Validation result data.
     */
    const setClientSideError = (validationResult) => {
        if (validationResult.errors.password) {
            setErrorMessage(validationResult.errors.password);
        }

        if (validationResult.errors.email) {
            setErrorMessage(validationResult.errors.email);
        }

        if (validationResult.errors.username) {
            setErrorMessage(validationResult.errors.username);
        }

        if (validationResult.errors.firstName) {
            setErrorMessage(validationResult.errors.firstName);
        }
        if (validationResult.errors.lastName) {
            setErrorMessage(validationResult.errors.lastName);
        }
        if (validationResult.errors.phone) {
            setErrorMessage(validationResult.errors.phone);
        }
        if (validationResult.errors.city) {
            setErrorMessage(validationResult.errors.city);
        }

        setShowAlertBar(true);
    };

    /**
     * Set server side error.
     *
     * Sets error data received as a response of our query from the server
     * and set statusbar to true so that its visible.
     *
     * @param {String} error Error
     *
     * @return {void}
     */
    const setServerSideError = (error) => {
        setErrorMessage(error);
        setShowAlertBar(true);
    };

    /**
     * Handles user registration.
     *
     * @param {object} event Event Object.
     * @param {object} registerCustomer registerCustomer function from REGISTER_USER mutation query.
     * @return {void}
     */
    const handleRegister = async (event, registerCustomer) => {
        if (process.browser) {
            event.preventDefault();

            // Validation and Sanitization.
            const validationResult = validateAndSanitizeRegisterForm({ username, email, password, firstName, lastName, phone, city });

            // If the data is valid.
            if (validationResult.isValid) {
                await registerCustomer({
                    variables: {
                        username: validationResult.sanitizedData.username,
                        email: validationResult.sanitizedData.email,
                        password: validationResult.sanitizedData.password,
                        firstName: validationResult.sanitizedData.firstName,
                        lastName: validationResult.sanitizedData.lastName,
                        phone: validationResult.sanitizedData.phone,
                        city: validationResult.sanitizedData.city
                    }
                })
                    .then((response) => handleRegisterSuccess(response))
                    .catch((error) => handleRegisterFail(error.graphQLErrors[0].message));


            } else {
                setClientSideError(validationResult);
            }
        }
    };

    /**
     * Handle Registration Fail.
     *
     * Set the error message text and validated to false.
     *
     * @param {String} err Error message received
     * @return {void}
     */
    const handleRegisterFail = (err) => {
        const error = err.split('_').join(' ').toUpperCase();

        setServerSideError(error);
    };

    /**
     * Handle Register success.
     *
     * @param {Object} response Response received.
     * @return {void}
     */
    const handleRegisterSuccess = (response) => {
        if (response.data.registerCustomer.customer.email) {
            // Set form fields value to empty.
            setErrorMessage('');
            setUsername('');
            setPassword('');

            localStorage.setItem('registration-success', 'yes');

            // Add a message.
            setSuccessMessage(
                'Inscription réussi! . Vous allez être redirigé vers la page de connexion maintenant...'
            );

            setTimeout(() => {
                // Send the user to Login page.
                Router.push('/login?registered=true');
            }, 3000);
        }
    };

    const [
        registerCustomer,
        {
            data: data,
            loading: loading,
            error: error,
        },
    ] = useMutation(REGISTER_USER, { client })

    return (
        <Layout>

            <Container className="connect">
                <div className='iner-enreg'>
                    {/* Title */}
                    <h2 className="mb-2">S'enregistrer</h2>

                    {/* Error Message */}
                    {'' !== errorMessage
                        ? showAlertBar && (
                            <MessageAlert
                                message={errorMessage}
                                success={false}
                                onCloseButtonClick={onCloseButtonClick}
                            />
                        )
                        : ''}

                    {'' !== successMessage
                        ? showAlertBar && (
                            <MessageAlert
                                message={successMessage}
                                success={true}
                                onCloseButtonClick={onCloseButtonClick}
                            />
                        )
                        : ''}

                    {/* Login Form */}
                    <form
                        className="mt-1"
                        onSubmit={(event) => handleRegister(event, registerCustomer)}>

                        {/* firstName */}
                        <div className="form-group">
                            <label className="lead mt-1" htmlFor="firstName">
                                Prénoms
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                id="firstName"
                                placeholder="Enter firstName"
                                value={firstName}
                                onChange={(event) => setFirstName(event.target.value)}
                            />
                        </div>
                        {/* lastName */}
                        <div className="form-group">
                            <label className="lead mt-1" htmlFor="lastName">
                                Nom
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                id="lastName"
                                placeholder="Enter lastName"
                                value={lastName}
                                onChange={(event) => setLastName(event.target.value)}
                            />
                        </div>

                        {/* phone */}
                        <div className="form-group">
                            <label className="lead mt-1" htmlFor="phone">
                                Téléphone
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                id="phone"
                                placeholder="Enter phone"
                                value={phone}
                                onChange={(event) => setPhone(event.target.value)}
                            />
                        </div>

                        {/* Ville */}
                        <div className="form-group">
                            <label className="lead mt-1" htmlFor="city">
                                Ville
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                id="city"
                                placeholder="Enter city"
                                value={city}
                                onChange={(event) => setCity(event.target.value)}
                            />
                        </div>

                        {/* Username */}
                        <div className="form-group">
                            <label className="lead mt-1" htmlFor="username">
                                Nom utilisateur
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                id="username"
                                placeholder="Enter username"
                                value={username}
                                onChange={(event) => setUsername(event.target.value)}
                            />
                        </div>

                        {/* Email */}
                        <div className="form-group">
                            <label className="lead mt-1" htmlFor="email">
                                Email
                            </label>
                            <input
                                type="email"
                                className="form-control"
                                id="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(event) => setEmail(event.target.value)}
                            />
                        </div>

                        {/* Password */}
                        <div className="form-group">
                            <label className="lead mt-1" htmlFor="password">
                                Mot de passe
                            </label>
                            <input
                                type="password"
                                className="form-control"
                                id="password"
                                placeholder="Enter password"
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                            />
                        </div>

                        {/* Submit Button */}
                        <div className="form-group sub">
                            <button
                                className="btn btn-primary"
                                disabled={loading ? 'disabled' : ''}
                                type="submit">
                                S'enregistrer
                            </button>
                            <Link href="/login">
                                <a className="btn btn-secondary ml-2">Connexion</a>
                            </Link>
                        </div>

                        {/*	Loading */}
                        {loading ? <Loading message={'en cours...'} /> : ''}
                    </form>
                </div>
            </Container>
        </Layout>
    );
};

export default Register;
