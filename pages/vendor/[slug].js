import Layout from "../../src/components/Layout";
import client from "../../src/components/ApolloClient";
import Product from "../../src/components/Product";
import { PRODUCT_BY_CATEGORY_SLUG, PRODUCT_CATEGORIES_SLUGS } from "../../src/queries/product-by-category";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import axios from "axios";

export default function VendorSingle(props) {

    const router = useRouter()

    // If the page is not yet generated, this will be displayed
    // initially until getStaticProps() finishes running
    if (router.isFallback) {
        return <div>Loading...</div>
    }

    const { categoryName, products } = props;
    console.warn(props);

    return (
        <Layout>
            <div className="product-categories-container container mx-auto my-32 px-4 xl:px-0">
                {categoryName ? <h3 className="text-2xl mb-5 uppercase">{categoryName}</h3> : ''}
                <div className="product-categories grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 gap-4">
                    <div>produit</div>
                </div>
            </div>
        </Layout>
    )
};

export async function getStaticProps(context) {

    const token = 'Basic YWRtaW46YWRpbmtyYTIyNQ==';

    const url = 'https://adinkra.bigfive.dev//wp-json/wcfmmp/v1/store-vendors/5/products';
    const headers = {
        'Authorization': `${token}`
    };

    const res = await fetch(url, {
        method: 'GET',
        headers: headers,
    })
    const data = await res.json()

    return {
        props: {
            categoryName: 'ulrich',
            products: data
        },
        revalidate: 1
    }
}

export async function getStaticPaths() {
    const pathsData = []
    pathsData.push({ params: { slug: 'ulrich' } })

    return {
        paths: pathsData,
        fallback: true
    }
}

