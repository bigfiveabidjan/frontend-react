import Layout from '../src/components/Layout';
import { useState, useEffect } from 'react';
import { isUserValidated,logoutUser } from '../src/utils/auth-functions';
import isEmpty from '../src/validator/isEmpty';
import Router from 'next/router';

/**
 * MyAccount functional component.
 *
 * @return {object} MyAccount content.
 */
const MyAccount = () => {
    const [showContent, setShowContent] = useState(false);
    const [userData, setUserData] = useState('');

    useEffect(() => {
        const userValidatedData = isUserValidated();

        if (!isEmpty(userValidatedData)) {
            setUserData(userValidatedData);
            setShowContent(true);
        } else {
            // If user is not logged in send the user back to login page.
            Router.push('/login');
        }
    }, []);
    const signOut = () => {
        if (process.browser) {
            logoutUser('/');
        }
      }
    return (
        <Layout>
            {/* Only Show Content if user is logged in */}
            {showContent ? (
                <div className="container mt-5 wpd-my-account">
                    <h4>Mon Compte</h4>
                 {/*    <div className="wpd-my-account-sidebar">
                        <a className="wpd-my-account-sidebar__link" href="#dashboard">
                            <i className="fa fa-fw fa-home"></i> Dashboard
                        </a>
                        <a className="wpd-my-account-sidebar__link" href="#orders">
                            <i className="fa fa-fw fa-wrench"></i> Orders
                        </a>
                        <a className="wpd-my-account-sidebar__link" href="#addresses">
                            <i className="fa fa-fw fa-user"></i> Adresses
                        </a>
                        <a className="wpd-my-account-sidebar__link" href="#account-details">
                            <i className="fa fa-fw fa-envelope"></i> Account Details
                        </a>
                    </div>*/}

                    <div className="wpd-my-account__main">
                   
                        <div id="dashboard">
                            {userData.user.nicename ? <h6>Bonjour {userData.user.nicename}!</h6> : ''}
                            <h5 className="mt-3">Détails</h5>
                            {userData.user.email ? <p>Email: {userData.user.email}</p> : ''}
                        </div>
                        <button onClick={() => signOut()}>Déconnexion</button>
                    </div>
                </div>
            ) : (
                ''
            )}
        </Layout>
    );
};

export default MyAccount;
