import Layout from "../src/components/Layout";
import Product from "../src/components/Product";
import client from '../src/components/ApolloClient';
import ParentCategoriesBlock from "../src/components/category/category-block/ParentCategoriesBlock";
import PRODUCTS_AND_CATEGORIES_QUERY from "../src/queries/product-and-categories";
import HeroCarousel from "../src/components/home/hero-carousel";
import "tailwindcss/tailwind.css"
import BanniereHome from "../src/components/home/banniereHome/BanniereHome";


export default function Home (props) {

	const {  products, productCategories, miseEnAvant, sliders, bannierePub, header, headerMenus, footerMenus, footer} = props || {};

	return (
		<div>
			<Layout header={header} headerMenus={headerMenus} footerMenus={footerMenus} footer={footer}>
				{/*Hero Carousel*/}
				<div className="slider-container container mx-auto  px-4 xl:px-0">
				<HeroCarousel sliders={ sliders } />
				</div>

				{/*Bannière pub*/}
				<div className="container mx-auto  px-4 xl:px-0">
				<BanniereHome bannierePub={bannierePub}/>
				</div>
				{/*Categories*/ }
				<div className="product-categories-container container mx-auto my-32 px-4 xl:px-0">
					<h2 className="main-title text-xl mb-5 uppercase"><span className="main-title-inner">Categories</span></h2>
					<ParentCategoriesBlock productCategories={ productCategories }/>
				</div>
				<h1 className="products-main-title welcome main-title mb-5 text-xl uppercase"><span className="main-title-inner">BIENVENUE SUR ADINKRA</span></h1>

				{/*Products 2*/ }
				<div className="products mise-en-avant container mx-auto my-32 px-5 xl:px-0">
					<h2 className="products-main-title main-title mb-5 text-xl uppercase"><span className="main-title-inner">Produits en avant</span></h2>
					<div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-5 gap-5">
						{ miseEnAvant.length ? (
							miseEnAvant.map( product => <Product key={ product.id } product={ product }/> )
						) : '' }
					</div>
				</div>
				{/*Products 2*/ }
				<div className="products container mx-auto my-32 px-5 xl:px-0">
					<h2 className="products-main-title main-title mb-5 text-xl uppercase"><span className="main-title-inner">Produits</span></h2>
					<div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-5 gap-5">
						{ products.length ? (
							products.map( product => <Product key={ product.id } product={ product }/> )
						) : '' }
					</div>
				</div>
			</Layout>
		</div>
	)
};


export async function getStaticProps () {

	const { data } = await client.query( {
		query: PRODUCTS_AND_CATEGORIES_QUERY,
	} );

	return {
		props: {
			productCategories: data?.productCategories?.nodes ? data.productCategories.nodes : [],
			products: data?.products?.nodes ? data.products.nodes : [],
			miseEnAvant: data?.miseEnAvant?.nodes ? data?.miseEnAvant?.nodes : [],
			sliders: data?.sliders?.nodes ? data?.sliders?.nodes : [],
			bannierePub: data?.banniereAfterSlider?.nodes ? data?.banniereAfterSlider?.nodes : [],
			header: data?.header,
			headerMenus: data?.headerMenus,
			footerMenus: data?.footerMenus,
			footer: data?.footer,
		},
		revalidate: 1
	}

};
