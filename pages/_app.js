import "../src/styles/style.scss";
import "../src/styles/main.scss";

// add bootstrap css 
import 'bootstrap/dist/css/bootstrap.css';
// own css files here
import "../src/css/customcss.css";

import { AuthProvider } from '../src/components/Auth'
import Router from 'next/router';
import NProgress from 'nprogress';

import SimpleReactLightbox from 'simple-react-lightbox';

NProgress.configure({ showSpinner: false });
Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps }) {
  return (
    <AuthProvider>
      <SimpleReactLightbox>
        <Component {...pageProps} />
      </SimpleReactLightbox>
    </AuthProvider>
  )
}

export default MyApp

