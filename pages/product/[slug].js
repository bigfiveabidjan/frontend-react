import Layout from '../../src/components/Layout';
import { useRouter } from 'next/router';
import client from '../../src/components/ApolloClient';
import AddToCartButton from '../../src/components/cart/AddToCartButton';
import {PRODUCT_BY_SLUG_QUERY, PRODUCT_SLUGS} from '../../src/queries/product-by-slug';
import { isEmpty } from 'lodash';
import GalleryCarousel from "../../src/components/single-product/gallery-carousel";
import Price from "../../src/components/single-product/price";
import React, { Component, useEffect, useState } from 'react';
import axios from 'axios';
import Image from 'next/image';
import Link from 'next/link';
import { Col, Row, Container, Button, Form } from 'react-bootstrap';
import ContactVendeur from '../../src/components/vendeur/ContactVendeur';
import SimilarProduct from '../../src/components/SimilarProduct';
import AvisVendeur from '../../src/components/vendeur/AvisVendeur';
import { SRLWrapper } from "simple-react-lightbox";
import Review from '../../src/components/vendeur/Review';
import Commente from '../../src/components/vendeur/Commente';
import { faComment } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const token = 'Basic YWRtaW46YWRpbmtyYTIyNQ==';

const Product = (props) => {

    const [store,setStore] = useState({});
    // Get it from props
    const { route } = props;

    //Information du produit à afficher
    const product = props.product;

    //Recuperation de la liste des produits pour afficher les produits similar
    const products = product.related.nodes;

    const produitId = props.product.productId;
    //information du produit avec l'auteur afin de recuperer les informations sur la boutique du vendeur
    axios.get('https://adinkra.bigfive.dev/wp-json/wcfmmp/v1/products/'+produitId, {
        headers: {
            'Authorization': `${token}`
        }})
        .then((res) => {
            setStore(res.data);
        })
        .catch((error) => {
            console.error(error)
        })

    //Recuperation des informations sur la boutique du vendeur

    return (
        <Layout titre={product.name}>
        { product ? (
            <Container>
                <Row className="images-produit">
                    <Col md="12">
                        <div className="product-images">
                            { !isEmpty( product?.galleryImages?.nodes ) ? (
                                <SRLWrapper>
                                    <GalleryCarousel gallery={product?.galleryImages?.nodes}/>
                                </SRLWrapper>
                            ) : !isEmpty( product.image ) ? (
                                <SRLWrapper>
                                    <img
                                        src={ product?.image?.sourceUrl }
                                        alt={ product.name }
                                        width="100%"
                                        height="auto"
                                        srcSet={ product?.image?.srcSet }
                                    />
                                </SRLWrapper>

                            ) : null }
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col md="8">
                        <Container>
                            <Row>
                                <Col md="12" className="titre-produit">
                                    <h4 className="products-main-title text-2xl uppercase">{ product.name }</h4>
                                    <Price salesPrice={product?.price} regularPrice={product?.regularPrice}/>  
                                    <hr/>
                                    <div className="grid md:grid-cols-1 gap-1"
                                            dangerouslySetInnerHTML={ {
                                                __html: product.description, 
                                            } }
                                            className="product-description mb-5"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col md="12" className="avis-vendeur">
                                    <h3>Rencontrez le vendeur</h3>
                                    <div className="separationDroite"></div>
                                </Col>
                                <Col md="4">
                                    <Row>
                                        <Col md="5">
                                            <Link href={{ pathname: '/vendor/'+store?.store?.vendor_shop_name}}>
                                                <a><img src={store?.store?.vendor_shop_logo} /></a>
                                            </Link>
                                        </Col>
                                        <Col md="7">
                                            <Link href={{ pathname: '/vendor/'+store?.store?.vendor_shop_name}}>
                                                <a><strong>{store?.store?.vendor_display_name ? store?.store?.vendor_display_name : ''}</strong><br></br>{'@'+store?.store?.vendor_shop_name}</a>
                                            </Link>
                                                <br></br><span>Note : {store?.store?.store_rating ? store?.store?.store_rating+'/5' : 'Aucune avis/note'}</span>
                                                <br></br>
                                                <Review store_rating={store?.store?.store_rating} />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md="8" className="avis-product">
                                    <h4>Note et Avis  {store.name}</h4>
                                    <span>{store.average_rating+"/5 "}</span>
                                    <Review store_rating={store.average_rating} />
                                    <span>{' ('+store.rating_count+')'}</span>
                                    <hr></hr>
                                        <Commente produitReview={product.reviews.edges} />
                                    </Col>
                                    {/*<AvisVendeur produit={product} vendeur={vendeur} averageRating={product.averageRating} reviewCount={product.reviewCount}/>*/}
                                </Row>
                                <Row className="similaire">
                                    <Col md="12" className="product-similar">
                                        <h3>Tu pourrais aussi aimer</h3>
                                        <div className="separationDroite"></div>
                                    </Col>
                                    <Col md="12">
                                        {/*Products similar*/ }
                                        
                                        { products.length > 0 ? (
                                            <SimilarProduct products={products}/>
                                        ) : <p>Aucun produit similaire</p> }
                                    

                                </Col>
                            </Row>
                        </Container>


                    </Col>
                    <Col md="4">
                        <div className="achat-produit">
                            <AddToCartButton product={ product }/>  
                        </div>
                        <div className="vendeur">
                        <Container className="contactVendor">
                            <Row>
                                <Col md={12} className="vendor-info">
                                    <Row className="justify-content-md-center">
                                        <Col md={3} className="vendro-avatar">
                                            <img src={store?.store?.vendor_shop_logo} />
                                        </Col>
                                        <Col md={9} className="vendor-name">
                                            <a><strong>{store?.store?.vendor_shop_name ? "@"+store?.store?.vendor_shop_name : ''}</strong></a>                                                 

                                            <Review store_rating={store?.store?.store_rating} />
                                            <hr/>
                                            <Link href={{ pathname: '/vendor/'+store?.store?.vendor_shop_name}}>
                                                <a>Voir la boutique du vendeur</a>
                                            </Link>
                                        </Col>
                                        <Col md={12}>
                                            <Button variant="danger"> <FontAwesomeIcon icon={faComment} /> Chat</Button>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={12} className="contact-form-vendeur">
                                    <Row className="justify-content-md-center">
                                        <Col md={12} className="">
                                            <Form>
                                                <Form.Group controlId="exampleForm.ControlTextarea1">
                                                    <Form.Control placeholder="Ecrivez votre demande" as="textarea" rows={3} />
                                                </Form.Group>
                                                <Form.Group controlId="formBasicEmail">
                                                    <Form.Control type="email" placeholder="Votre email" />
                                                </Form.Group>
                                                <Form.Group controlId="formBasicCheckbox">
                                                    <Form.Control type="text" placeholder="Votre nom" />
                                                </Form.Group>
                                                <Button variant="warning" type="submit">
                                                    {/*Envoyez le message à {'@'+vendor?.name}*/}
                                                </Button>
                                            </Form>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>

                        </div>
                    </Col>
                </Row>
            </Container>
        ) : (
            ''
        ) }
    </Layout>
    )
}


const product = (props) => {
    const { product, products } = props;
    const router = useRouter();

    if (router.isFallback) {
        return <div>Loading...</div>
    }
  
    return <Product {...props} router={router} />;
};
export default  product;

export async function getStaticProps(context) {

    const {params: { slug }} = context

    const {data} = await client.query({
        query: PRODUCT_BY_SLUG_QUERY,
        variables: { slug }
    })

    return {
        props: {
            product: data?.product || {},
            products: data?.product?.productCategories?.nodes || {},
        },
        revalidate: 1
    };
}

export async function getStaticPaths() {
    const { data } = await client.query({
        query: PRODUCT_SLUGS
    })

    const pathsData = []

    data?.products?.nodes && data?.products?.nodes.map((product) => {
        if (!isEmpty(product?.slug)) {
            pathsData.push({ params: { slug: product?.slug } })
        }
    })

    return {
        paths: pathsData,
        fallback: true
    }
}